class HomeController < ApplicationController
  # skip_before_action :authenticate_user!, :only => [:index]
  before_action :authenticate_user!

  def index
    session[:conversations] ||= []

    @users = User.all.where.not(id: current_user)
  end
end

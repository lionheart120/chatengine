class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  # before_action :authenticate_user!
  include Accessible
  before_action :check_user


  def about
  end


  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name,:city,:phone_number,:email,:password])
    devise_parameter_sanitizer.permit(:account_update, keys: [:email,:password,:name,:city,])
  end
end

class DashboardController < ApplicationController
  # before_action :authenticate_user!
  include Accessible
  before_action :check_user


  def index
    @users = User.all
  end


  private


  def user_params
    accessible = [:username, :email] # extend with your own params
    accessible << [:password, :password_confirmation] unless params[:user][:password].blank?
    params.require(:user).permit(accessible)
  end



  def secure_params
    params.require(:user).permit(:name,:email,:password,:city,)
  end
end

class User < ApplicationRecord
  # Include default admin modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable,:trackable, :validatable

  after_create :send_admin_mail
  scope :all_except, ->(user) { where.not(id: user) }

  # has_many :messages, dependent: :destroy
  # has_many :conversations, :foreign_key => :sender_id

  def send_admin_mail
    UserMailer.send_welcome_email(self).deliver_later
  end


  has_many :authored_conversations, class_name: 'Conversation', foreign_key: 'author_id'
  has_many :received_conversations, class_name: 'Conversation', foreign_key: 'receiver_id'

  has_many :messages, dependent: :destroy

  def name
    email.split('@')[0]
  end
end

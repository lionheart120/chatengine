class Message < ApplicationRecord

  # belongs_to :user
  # belongs_to :conversation
  # validates_presence_of :body, :conversation_id, :user_id
  #
  def message_time
    created_at.strftime("%m/%d/%y at %l:%M %p")
  end

  belongs_to :conversation
  belongs_to :user

  validates :body, presence: true

  # validates :body, presence: true, unless: :attachment_data

  # after_create_commit :broadcast_message
  #
  # include AttachmentUploader[:attachment]
  #
  # # virtual attributes for temp storage of attachment's original name
  # def attachment_name=(name)
  #   @attachment_name = name
  # end
  #
  # def attachment_name
  #   @attachment_name
  # end
  #
  # private
  #
  # def broadcast_message
  #   MessageBroadcastJob.perform_later(self)
  # end
end

class UserMailer < ApplicationMailer

  default from: 'anjum.zaman808@gmail.com'

  def send_welcome_email(user)
    @user = user
    mail(:to => @user.email, :subject => "Dear User Welcome")
  end

end

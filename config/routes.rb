Rails.application.routes.draw do

  devise_for :admins
  get 'admin/index', to: 'admin#index', as: 'admin'
  devise_for :users, controllers: { registrations: 'user/registrations' }

  resources :users, only: [:index,:destroy]
  resources :messages, only: [:new, :create]
  resources :conversations, only: [:index, :show]

  root to: 'conversations#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
